Requirements
---

Here are some requirements derived from the conversation written on the [README](./README.md) 

### **General description**
Membuat sistem informasi asisten dosen untuk membantu pihak fakultas dan 
mahasiswa.

### **Roles**
Terdapat beberapa peran yang disebutkan dalam cerita, diantaranya yakni:
1. Asisten Dosen
2. Pihak Fakultas
3. Mahasiswa

### **Features**
Terdapat beberapa fitur yang disediakan oleh aplikasi ini dalam memudahkan
koordinasi antara pihak fakultas dan mahasiswa. Di antara fitur tersebut
yaitu:
1. Fitur Lowongan `MATA KULIAH`
    - **Pihak Fakultas** dapat menawarkan lowongan `MATA KULIAH` kepada 
      **Mahasiswa** yang berminat untuk menjadi **Asisten Dosen**
    - Satu lowongan `MATA KULIAH` dapat menerima banyak mahasiswa
    - Semua **Mahasiswa** yang mendaftar pada satu `MATA KULIAH` akan
      langsung diterima
    - `MATA KULIAH` yang sudah memiliki **Asisten Dosen** tidak dapat dihapus
    - **Mahasiswa** yang sudah menjadi **Asisten Dosen** pada suatu `MATA KULIAH` 
      juga tidak dapat dihapus
2. Fitur pencatatan `LOG`
    - Setiap pekerjaan **Asisten Dosen** akan dicatat dalam sebuah `LOG`
    - **Asisten Dosen** dapat membuat (CREATE), memperbaharui (UPDATE), dan
      menghapus (DELETE) `LOG`
    - **Asisten Dosen** juga dapat melihat (READ) _summary_ laporan dari pekerjaan
      mereka pada bulan tertentu, dan jumlah uang yang diterima berdasarkan
      jumlah `LOG`
    - Jumlah uang yang diterima tiap bulannya dihitung dengan rumus 350 Greil
      per jam
    - Sebuah `LOG` dapat berisikan data lebih singkat dari satu jam
   
### **Assumptions**
Selain itu, terdapat pula beberapa asumsi yang saya ambil dalam mengerjakan aplikasi 
ini, diantaranya adalah:
1. **Mahasiswa** hanya bisa menjadi **Asisten Dosen** satu `MATA KULIAH` dalam satu waktu

### **Implementations**
_Snippet_ bentuk implementasi yang dijalankan berdasarkan _requirements_ yang dibuat
1. Terdapat objek `LOG SUMMARY` untuk tiap-tiap mahasiswa

### **Useful Links**
1. Spring + PSQL + JDBC setup
   - https://dzone.com/articles/bounty-spring-boot-and-postgresql-database
   - https://zetcode.com/springboot/postgresql/
   - https://mkyong.com/spring-boot/spring-boot-spring-data-jpa-postgresql/
2. JPA
   - https://stackabuse.com/guide-to-jpa-with-hibernate-basic-mapping/
   - https://stackabuse.com/a-guide-to-jpa-with-hibernate-relationship-mapping/