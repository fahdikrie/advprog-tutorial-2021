package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;

import java.util.List;

public interface LogService {
    Log createLog(String npm, Log log);
    Log getLog(int idLog);
    Log updateLog(int idLog, Log log);
    Log deleteLog(int idLog);
    List<Log> getListLog();
    List<Log> getListLogByMahasiswa(String npm);
}
