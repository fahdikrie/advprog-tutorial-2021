package csui.advpro2021.tais.service;

import csui.advpro2021.tais.core.LogSummary;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LogServiceImpl implements LogService{
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaServiceImpl mahasiswaService;

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        // define relation of a log to an instance of mahasiswa
        log.setMahasiswa(mahasiswa);
        // add log to mahasiswa's logList
        mahasiswa.getLogList().add(log);
        // save/update instances on repository
        logRepository.save(log);
        mahasiswaRepository.save(mahasiswa);
        return log;
    }

    @Override
    public Log getLog(int idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public Log updateLog(int idLog, Log log) {
        return null;
    }

    @Override
    public Log deleteLog(int idLog) {
        return null;
    }

    @Override
    public List<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public List<Log> getListLogByMahasiswa(String npm) {
        return logRepository
                .findAll()
                .stream()
                .filter(
                    log -> log.getMahasiswa().getNpm().equals(npm)
                ).collect(Collectors.toList());
    }
}
