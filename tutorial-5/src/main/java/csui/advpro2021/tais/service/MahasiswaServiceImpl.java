package csui.advpro2021.tais.service;

import csui.advpro2021.tais.core.LogSummary;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        mahasiswa.setNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        mahasiswaRepository.deleteById(npm);
    }

    @Override
    public Mahasiswa createAsistenDosen(String npm, String kodeMatkul) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        MataKuliah mataKuliah = mataKuliahRepository.findByKodeMatkul(kodeMatkul);
        mahasiswa.setMataKuliah(mataKuliah);
        mataKuliah.getAsistenDosenList().add(mahasiswa);
        mahasiswaRepository.save(mahasiswa);
        mataKuliahRepository.save(mataKuliah);
        return mahasiswa;
    }

    @Override
    public List<LogSummary> getListSummary(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        return LogSummary.getLogSummaryList(mahasiswa);
    }

    @Override
    public LogSummary getSummaryByMonth(String npm, String month) {
        Mahasiswa mahasiswa = this.getMahasiswaByNPM(npm);
        LogSummary logSummary = LogSummary.getLogSummaryList(mahasiswa)
                .stream()
                .filter(
                        lsm -> lsm.getMonth().equalsIgnoreCase((month))
                ).findFirst().orElse(null);
        return logSummary;
    }
}
