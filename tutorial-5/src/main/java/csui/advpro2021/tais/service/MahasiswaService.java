package csui.advpro2021.tais.service;

import csui.advpro2021.tais.core.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;

import java.util.List;
import java.util.Map;

public interface MahasiswaService {
    Mahasiswa createMahasiswa(Mahasiswa mahasiswa);

    Iterable<Mahasiswa> getListMahasiswa();

    Mahasiswa getMahasiswaByNPM(String npm);

    Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa);

    void deleteMahasiswaByNPM(String npm);

    Mahasiswa createAsistenDosen(String npm, String kodeMatkul);

    List<LogSummary> getListSummary(String npm);

    LogSummary getSummaryByMonth(String npm, String month);
}
