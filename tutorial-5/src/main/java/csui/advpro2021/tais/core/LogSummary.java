package csui.advpro2021.tais.core;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class untuk menyimpan objek summary
 * dengan fungsionalitas:
 * 1. Mengembalikan int summary gaji dalam
 *    satu bulan
 * 3. Hitung total jamKerja
 * 4. Hitung total gaji
 */

@Data
@Setter
@Getter
public class LogSummary {
    private static int pembayaran = 350;
    private String month;
    private int jamKerja;
    private long totalGaji;

    public LogSummary(String month, int jamKerja, long totalGaji) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.totalGaji = totalGaji;
    }

    /**
     * Mengupdate LogSummary mahasiswa/asisten dosen
     * setiap ada penambahan log baru
     */
    public static List<LogSummary> getLogSummaryList(Mahasiswa mahasiswa) {
        List<Log> logList = mahasiswa.getLogList();
        int[] totalJamKerjaTiapBulan = new int[12];
        long[] totalGajiTiapBulan = new long[12];

        for (Log log : logList) {
            int monthValue = log.getStart().getMonthValue() - 1;
            totalJamKerjaTiapBulan[monthValue] += log.getJamKerja();
            totalGajiTiapBulan[monthValue] = (long) totalJamKerjaTiapBulan[monthValue] * pembayaran;
        }

        List<LogSummary> logSummaryList = new LinkedList<>();

        Calendar cal = Calendar.getInstance();
        for (int i = 0; i < 12; i++) {
            cal.set(Calendar.MONTH, i);
            String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
            logSummaryList.add(new LogSummary(month, totalJamKerjaTiapBulan[i], totalGajiTiapBulan[i]));
        }

        return logSummaryList;
    }
}
