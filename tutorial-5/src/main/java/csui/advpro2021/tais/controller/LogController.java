package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.core.LogSummary;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * [Requirements]
 * Untuk Log
 * 1. Create/Post Log
 * 2. Read/Get Log
 * 3. Update Log
 * 4. Delete Log
 * 5. Read/Get All Log
 * 6. Read/Get All log by Mahasiswa
 * Untuk Summary
 * 1. Get Summary Total
 * 2. Get Summary Perbulan
 */

@RestController
@RequestMapping("/log")
public class LogController {
    @Autowired
    private LogServiceImpl logService;

    @Autowired
    private MahasiswaServiceImpl mahasiswaService;

    @PostMapping(
        path = "/{npm}",
        produces = {"application/json"}
    )
    @ResponseBody
    public ResponseEntity postLog(
        @PathVariable(value = "npm") String npm,
        @RequestBody Log log
    ) {
        return ResponseEntity.ok(logService.createLog(npm, log));
    }

    @GetMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "idLog") int idLog) {
        Log log = logService.getLog(idLog);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @PutMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") int idLog, @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(idLog, log));
    }

    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") int idLog) {
        logService.deleteLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @GetMapping(
        path = "list/{npm}",
        produces = {"application/json"}
    )
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLogByMahasiswa(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getListLogByMahasiswa(npm));
    }

    @GetMapping(
            path = "summary/{npm}",
            produces = {"application/json"}
    )
    @ResponseBody
    public ResponseEntity getListSummary(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(mahasiswaService.getListSummary(npm));
    }

    @GetMapping(
            path = "summary/{npm}/{month}",
            produces = {"application/json"}
    )
    @ResponseBody
    public ResponseEntity getSummaryByMonth(
        @PathVariable(value = "npm") String npm,
        @PathVariable(value = "month") String month
    ) {
        return ResponseEntity.ok(mahasiswaService.getSummaryByMonth(npm, month));
    }
}
