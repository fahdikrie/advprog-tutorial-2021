package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Models untuk LOG
 * Field:
 *  1. Mahasiswa mahasiswa (one-to-many)
 *  2. int idLog [PK]
 *  3. LocalDateTime start
 *  4. LocalDateTime end
 *  5. String description
 */

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {

    @ManyToOne
    @JoinColumn(name = "npm", nullable = false)
    @JsonBackReference
    private Mahasiswa mahasiswa;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_log")
    private int idLog;

    @Column(name = "start_datetime")
    private LocalDateTime start;

    @Column(name = "end_datetime")
    private LocalDateTime end;

    @Column(name = "description")
    private String description;

    public Log(Mahasiswa mahasiswa, int idLog, LocalDateTime start, LocalDateTime end, String description) {
        this.mahasiswa = mahasiswa;
        this.idLog = idLog;
        this.start = start;
        this.end = end;
        this.description = description;
    }

    public long getJamKerja() {
        return Duration.between(this.start, this.end).toMinutes();
    }
}
