Singleton — Lazy Instantiation vs Eager Instantiation 
=====================================================

### Lazy Instantiation
The usage of singleton pattern with lazy instantiation means that 
the single object of a class is being instantiated once the object is 
called from another class. Before any call happens, the value of the 
object is defined (directly/indirectly) as null.

However, this approach has its own problem when implemented in a multithreaded
environment. Whereas upon multiple calling of this object within several other classes,
there could be a case where multiple objects of a singleton class are created at the same time.

### Eager Instantiation
Based on the caveats that came from the implementation of singleton pattern with lazy
instantiation, there is another solution called eager instantiation. Eager instantiation 
is a very straightforward solution, in which the instantiation of a singleton class object 
is being done right away on the callers' instance field.