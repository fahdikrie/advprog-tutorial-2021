package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredientsfactory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;

public class InuzumaRamenFactory implements IngredientsFactory {
    @Override
    public Flavor createFlavor() {
        return new Spicy();
    }

    @Override
    public Meat createMeat() {
        return new Pork();
    }

    @Override
    public Noodle createNoodle() {
        return new Ramen();
    }

    @Override
    public Topping createTopping() {
        return new BoiledEgg();
    }
}
