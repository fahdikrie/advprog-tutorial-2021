package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChickenTest {

    @InjectMocks
    private Chicken chicken;

    @BeforeEach
    public void setUp() throws Exception {
        chicken = new Chicken();
    }

    @Test
    public void testChickenCallGetDescriptionMethod() {
        assertEquals("Adding Wintervale Chicken Meat...", chicken.getDescription());
    }
}
