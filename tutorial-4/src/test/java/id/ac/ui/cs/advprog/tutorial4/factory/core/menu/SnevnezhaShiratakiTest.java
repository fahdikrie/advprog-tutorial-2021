package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiTest {

    @InjectMocks
    private SnevnezhaShirataki snevnezhaShirataki;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShirataki = new SnevnezhaShirataki("snevnezhaShirataki");
    }

    @Test
    public void testSnevnezhaShiratakiCallGetName() {
        assertEquals("snevnezhaShirataki", snevnezhaShirataki.getName());
    }
}
