package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeefTest {

    @InjectMocks
    private Beef beef;

    @BeforeEach
    public void setUp() throws Exception {
        beef = new Beef();
    }

    @Test
    public void testBeefCallGetDescriptionMethod() {
        assertEquals("Adding Maro Beef Meat...", beef.getDescription());
    }
}
