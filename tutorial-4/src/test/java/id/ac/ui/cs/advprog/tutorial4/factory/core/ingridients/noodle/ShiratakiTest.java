package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiratakiTest {

    @InjectMocks
    private Shirataki shirataki;

    @BeforeEach
    public void setUp() throws Exception {
        shirataki = new Shirataki();
    }

    @Test
    public void testShiratakiCallGetDescriptionMethod() {
        assertEquals("Adding Snevnezha Shirataki Noodles...", shirataki.getDescription());
    }
}
