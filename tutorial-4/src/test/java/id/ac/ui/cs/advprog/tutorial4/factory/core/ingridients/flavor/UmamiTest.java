package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UmamiTest {

    @InjectMocks
    private Umami umami;

    @BeforeEach
    public void setUp() throws Exception {
        umami = new Umami();
    }

    @Test
    public void testUmamiCallGetDescriptionMethod() {
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }
}
