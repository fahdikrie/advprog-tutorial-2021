package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheeseTest {

    @InjectMocks
    private Cheese cheese;

    @BeforeEach
    public void setUp() throws Exception {
        cheese = new Cheese();
    }

    @Test
    public void testCheeseCallGetDescriptionMethod() {
        assertEquals("Adding Shredded Cheese Topping...", cheese.getDescription());
    }
}
