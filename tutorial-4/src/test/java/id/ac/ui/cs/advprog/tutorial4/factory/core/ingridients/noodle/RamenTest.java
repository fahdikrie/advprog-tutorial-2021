package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RamenTest {

    @InjectMocks
    private Ramen ramen;

    @BeforeEach
    public void setUp() throws Exception {
        ramen = new Ramen();
    }

    @Test
    public void testRamenCallGetDescriptionMethod() {
        assertEquals("Adding Inuzuma Ramen Noodles...", ramen.getDescription());
    }
}
