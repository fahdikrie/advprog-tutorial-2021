package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoiledEggTest {

    @InjectMocks
    private BoiledEgg boiledEgg;

    @BeforeEach
    public void setUp() throws Exception {
        boiledEgg = new BoiledEgg();
    }

    @Test
    public void testBoiledEggCallGetDescriptionMethod() {
        assertEquals("Adding Guahuan Boiled Egg Topping", boiledEgg.getDescription());
    }
}
