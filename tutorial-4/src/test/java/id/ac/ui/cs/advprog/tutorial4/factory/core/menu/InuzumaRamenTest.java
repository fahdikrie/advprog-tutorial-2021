package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InuzumaRamenTest {

    @InjectMocks
    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamen = new InuzumaRamen("inuzumaRamen");
    }

    @Test
    public void testInuzumaRamenCallGetName() {
        assertEquals("inuzumaRamen", inuzumaRamen.getName());
    }
}
