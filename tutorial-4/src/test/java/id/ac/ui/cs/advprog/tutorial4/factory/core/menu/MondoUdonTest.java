package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonTest {

    @InjectMocks
    private MondoUdon mondoUdon;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdon = new MondoUdon("mondoUdon");
    }

    @Test
    public void testMondoUdonCallGetName() {
        assertEquals("mondoUdon", mondoUdon.getName());
    }
}
