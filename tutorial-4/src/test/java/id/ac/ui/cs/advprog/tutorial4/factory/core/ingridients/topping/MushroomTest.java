package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MushroomTest {

    @InjectMocks
    private Mushroom mushroom;

    @BeforeEach
    public void setUp() throws Exception {
        mushroom = new Mushroom();
    }

    @Test
    public void testMushroomCallGetDescriptionMethod() {
        assertEquals("Adding Shiitake Mushroom Topping...", mushroom.getDescription());
    }
}
