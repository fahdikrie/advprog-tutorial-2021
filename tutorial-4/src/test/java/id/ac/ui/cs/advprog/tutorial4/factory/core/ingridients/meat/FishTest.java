package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {

    @InjectMocks
    private Fish fish;

    @BeforeEach
    public void setUp() throws Exception {
        fish = new Fish();
    }

    @Test
    public void testFishCallGetDescriptionMethod() {
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }
}
