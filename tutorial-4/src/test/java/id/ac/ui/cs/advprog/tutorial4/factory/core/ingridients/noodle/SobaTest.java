package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SobaTest {

    @InjectMocks
    private Soba soba;

    @BeforeEach
    public void setUp() throws Exception {
        soba = new Soba();
    }

    @Test
    public void testSobaCallGetDescriptionMethod() {
        assertEquals("Adding Liyuan Soba Noodles...", soba.getDescription());
    }
}
