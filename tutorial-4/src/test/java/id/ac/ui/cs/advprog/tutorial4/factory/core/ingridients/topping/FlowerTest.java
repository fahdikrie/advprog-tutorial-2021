package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlowerTest {

    @InjectMocks
    private Flower flower;

    @BeforeEach
    public void setUp() throws Exception {
        flower = new Flower();
    }

    @Test
    public void testFlowerCallGetDescriptionMethod() {
        assertEquals("Adding Xinqin Flower Topping...", flower.getDescription());
    }
}
