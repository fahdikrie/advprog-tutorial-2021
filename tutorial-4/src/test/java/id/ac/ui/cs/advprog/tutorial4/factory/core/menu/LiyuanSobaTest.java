package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaTest {

    @InjectMocks
    private LiyuanSoba liyuanSoba;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSoba = new LiyuanSoba("liyuanSoba");
    }

    @Test
    public void testLiyuanSobaCallGetName() {
        assertEquals("liyuanSoba", liyuanSoba.getName());
    }
}
