package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SweetTest {

    @InjectMocks
    private Sweet sweet;

    @BeforeEach
    public void setUp() throws Exception {
        sweet = new Sweet();
    }

    @Test
    public void testSweetCallGetDescriptionMethod() {
        assertEquals("Adding a dash of Sweet Soy Sauce...", sweet.getDescription());
    }
}
