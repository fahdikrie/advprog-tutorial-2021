package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UdonTest {

    @InjectMocks
    private Udon udon;

    @BeforeEach
    public void setUp() throws Exception {
        udon = new Udon();
    }

    @Test
    public void testUdonCallGetDescriptionMethod() {
        assertEquals("Adding Mondo Udon Noodles...", udon.getDescription());
    }
}
