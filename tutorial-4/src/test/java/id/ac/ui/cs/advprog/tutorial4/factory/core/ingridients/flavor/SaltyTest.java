package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SaltyTest {

    @InjectMocks
    private Salty salty;

    @BeforeEach
    public void setUp() throws Exception {
        salty = new Salty();
    }

    @Test
    public void testSaltyCallGetDescriptionMethod() {
        assertEquals("Adding a pinch of salt...", salty.getDescription());
    }
}
