package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpicyTest {

    @InjectMocks
    private Spicy spicy;

    @BeforeEach
    public void setUp() throws Exception {
        spicy = new Spicy();
    }

    @Test
    public void testSpicyCallGetDescriptionMethod() {
        assertEquals("Adding Liyuan Chili Powder...", spicy.getDescription());
    }
}
