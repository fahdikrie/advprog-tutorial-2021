package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class OrderServiceImplTest {

    @InjectMocks
    private OrderService orderService = new OrderServiceImpl();

    @Test
    public void testUpdateDrinkWhenOrderingDrink() {
        orderService.orderADrink("Drink");
        assertEquals("Drink", OrderDrink.getInstance().getDrink());
    }

    @Test
    public void testUpdateFoodWhenOrderingFood() {
        orderService.orderAFood("Ramen");
        assertEquals("Ramen", OrderFood.getInstance().getFood());
    }


}
