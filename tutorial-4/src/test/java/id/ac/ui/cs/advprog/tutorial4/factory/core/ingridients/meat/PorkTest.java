package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PorkTest {

    @InjectMocks
    private Pork pork;

    @BeforeEach
    public void setUp() throws Exception {
        pork = new Pork();
    }

    @Test
    public void testPorkCallGetDescriptionMethod() {
        assertEquals("Adding Tian Xu Pork Meat...", pork.getDescription());
    }
}
