package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        // DONE: complete me
        return "Festering greed normal attack!";
    }

    @Override
    public String chargedAttack() {
        // DONE: complete me
        return "Festering greed charged attack!";
    }

    @Override
    public String getName() {
        return "Festering Greed";
    }

    @Override
    public String getHolderName() { return holderName; }
}
