package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// DONE: complete me :)
public class BowAdapter implements Weapon {

    private final Bow bow;
    private boolean isAimShot;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.isAimShot = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        isAimShot = !isAimShot;

        if (isAimShot) {
            return "Entered aim shot mode";
        } else {
            return "Left aim shot mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // DONE: complete me
        return bow.getHolderName();
    }
}
