package id.ac.ui.cs.advprog.tutorial3.facade.core.transformationfacade;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public interface TransformationFacade  {
    Spell encode(Spell spell);
    Spell decode(Spell spell);
}