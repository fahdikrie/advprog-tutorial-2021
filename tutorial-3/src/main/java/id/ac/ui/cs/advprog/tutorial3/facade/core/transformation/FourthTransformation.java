package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import com.sun.tools.javac.jvm.Code;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
 * The fourth transformation is using Caesar Cipher Algorithm
 * As suggested on the problem docs
 * Algorithm is referring to: https://www.baeldung.com/java-caesar-cipher
 */
public class FourthTransformation implements Transformation {
    private final int shift;

    public FourthTransformation(int shift) {
        this.shift = shift;
    }

    public FourthTransformation() {
        this(5);
    }

    @Override
    public Spell encode(Spell spell) {
        String spellText = spell.getText();
        Codex spellCodex = spell.getCodex();
        String result = this.cipher(
                spellText,
                this.shift,
                spellCodex.getCharSize()
        );

        return new Spell(result, spellCodex);
    }

    @Override
    public Spell decode(Spell spell) {
        String spellText = spell.getText();
        Codex spellCodex = spell.getCodex();
        String result = this.cipher(
                spellText,
                -this.shift,
                spellCodex.getCharSize() - (this.shift % spellCodex.getCharSize())
        );

        return new Spell(result, spellCodex);
    }

    public String cipher(String spellText, int shift, int codexLength) {
        StringBuilder result = new StringBuilder();

        for (char character : spellText.toCharArray()) {
            if (character != ' ') {
                int originalAlphabetPosition = character - 'a';
                int newAlphabetPosition = (originalAlphabetPosition + shift) % codexLength;
                char newCharacter = (char) ('a' + newAlphabetPosition);
                result.append(newCharacter);
            } else {
                result.append(character);
            }
        }

        return result.toString();
    }
}
