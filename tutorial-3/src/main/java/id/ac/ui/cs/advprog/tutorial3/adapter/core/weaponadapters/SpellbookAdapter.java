package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// DONE: complete me :)
public class SpellbookAdapter implements Weapon {

    private final Spellbook spellbook;
    private boolean hasChargedAttack;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.hasChargedAttack = false;
    }

    @Override
    public String normalAttack() {
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!this.hasChargedAttack) {
            this.hasChargedAttack = true;
            return spellbook.largeSpell();
        }
        else {
            return "Can't do charged attack twice in a row pal";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
