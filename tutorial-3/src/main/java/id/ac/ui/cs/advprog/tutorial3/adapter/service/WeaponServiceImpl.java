package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// DONE: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;

    // DONE: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> weapons = weaponRepository.findAll();

        for (Bow bow: bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                weapons.add(new BowAdapter(bow));
            }
        }

        for (Spellbook spellbook: spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                weapons.add(new SpellbookAdapter(spellbook));
            }
        }

        weapons.sort(Comparator.comparing(Weapon::getHolderName));
        return weapons;
    }

    // DONE: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = this.getWeaponByAlias(weaponName);
        StringBuilder sb = new StringBuilder();
        weaponRepository.save(weapon);

        sb.append(weapon.getHolderName())
          .append(" attacked with ")
          .append(weapon.getName());

        switch (attackType) {
            case 1:
                sb.append(" normal attack: ")
                  .append(weapon.normalAttack());
                break;
            case 2:
                sb.append(" charged attack: ")
                  .append(weapon.chargedAttack());
                break;
            default:
                break;
        }

        logRepository.addLog(sb.toString());
        sb.setLength(0);
    }

    public Weapon getWeaponByAlias(String weaponName) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if (weapon != null) {
            return weapon;
        }

        Bow bow = bowRepository.findByAlias(weaponName);
        if (bow != null) {
            return (new BowAdapter(bow));
        }

        Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
        if (spellbook != null) {
            return (new SpellbookAdapter(spellbook));
        }

        return null;
    }

    // DONE: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}