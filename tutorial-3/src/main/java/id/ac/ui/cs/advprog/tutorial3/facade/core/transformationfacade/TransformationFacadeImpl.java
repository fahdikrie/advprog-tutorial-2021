package id.ac.ui.cs.advprog.tutorial3.facade.core.transformationfacade;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FourthTransformation;


public class TransformationFacadeImpl implements TransformationFacade {
    private final CelestialTransformation celestialTransformation;
    private final AbyssalTransformation abyssalTransformation;
    private final FourthTransformation fourthTransformation;

    public TransformationFacadeImpl(){
        this.celestialTransformation = new CelestialTransformation();
        this.abyssalTransformation = new AbyssalTransformation();
        this.fourthTransformation = new FourthTransformation();
    }

    @Override
    public Spell encode(Spell spell) {
        this.celestialTransformation.encode(spell);
        this.abyssalTransformation.encode(spell);
        this.fourthTransformation.encode(spell);

        return CodexTranslator.translate(spell, RunicCodex.getInstance());
    }

    @Override
    public Spell decode(Spell spell) {
        this.fourthTransformation.decode(spell);
        this.abyssalTransformation.decode(spell);
        this.celestialTransformation.decode(spell);

        return CodexTranslator.translate(spell, AlphaCodex.getInstance());
    }
}
