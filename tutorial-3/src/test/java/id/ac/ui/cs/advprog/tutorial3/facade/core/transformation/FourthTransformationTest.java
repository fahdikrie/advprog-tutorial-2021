package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FourthTransformationTest {
    private Class<?> fourthTransformation;

    @BeforeEach
    public void setup() throws Exception {
        fourthTransformation = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FourthTransformation");
    }

    @Test
    public void testFourthHasEncodeMethod() throws Exception {
        Method translate = fourthTransformation.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFourthEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Xfknwf fsi N |jsy yt f gqfhpxrnym yt ktwlj tzw x|twi";

        Spell result = new FourthTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testFourthEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "]kps|k kxn S \u0081ox~ ~y k lvkmu}ws~r ~y py|qo y\u007F| }\u0081y|n";

        Spell result = new FourthTransformation(10).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testFourthHasDecodeMethod() throws Exception {
        Method translate = fourthTransformation.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFourthDecodesCorrectly() throws Exception {
        String text = "Xfknwf fsi N |jsy yt f gqfhpxrnym yt ktwlj tzw x|twi";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new FourthTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testFourthDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "]kps|k kxn S \u0081ox~ ~y k lvkmu}ws~r ~y py|qo y\u007F| }\u0081y|n";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new FourthTransformation(10).decode(spell);
        assertEquals(expected, result.getText());
    }
}
