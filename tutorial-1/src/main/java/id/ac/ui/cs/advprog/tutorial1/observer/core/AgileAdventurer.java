package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //DONE: Complete Me
        this.guild = guild;
        this.guild.add(this);
    }

    //DONE: Complete Me
    @Override
    public void update() {
        // Agile => Delivery & Rumble
        String quest = guild.getQuestType();

        if (quest.equals("D") || quest.equals("R")) {
            this.getQuests().add(guild.getQuest());
        }
    }
}
