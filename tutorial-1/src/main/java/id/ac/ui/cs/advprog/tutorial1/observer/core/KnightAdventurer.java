package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //DONE: Complete Me
        this.guild = guild;
        this.guild.add(this);
    }

    //DONE: Complete Me
    @Override
    public void update() {
        // Knight => Delivery, Rumble, & Escort
        Quest quest = guild.getQuest();

        if (quest.getType().equals("D") || quest.getType().equals("R") || quest.getType().equals("E")) {
            this.getQuests().add(guild.getQuest());
        }
    }
}
