package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    //DONE: Complete me
    @Override
    public String attack() {
        return "Gun!";
    }

    @Override
    public String getType() {
        return "Attack with gun";
    }
}
