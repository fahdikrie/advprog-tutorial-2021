package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //DONE: Complete me
    @Override
    public String defend() {
        return "Shield!";
    }

    @Override
    public String getType() {
        return "Defend with shield";
    }}
