package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //DONE: Complete me
    @Override
    public String defend() {
        return "Barrier!";
    }

    @Override
    public String getType() {
        return "Defend with barrier";
    }}
