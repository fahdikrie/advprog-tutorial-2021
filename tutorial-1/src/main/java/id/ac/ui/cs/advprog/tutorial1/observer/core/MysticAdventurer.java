package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //DONE: Complete Me
        this.guild = guild;
        this.guild.add(this);
    }

    //DONE: Complete Me
    @Override
    public void update() {
        // Mythic => Delivery & Escort
        String quest = guild.getQuestType();

        if (quest.equals("D") || quest.equals("E")) {
            this.getQuests().add(guild.getQuest());
        }
    }
}
