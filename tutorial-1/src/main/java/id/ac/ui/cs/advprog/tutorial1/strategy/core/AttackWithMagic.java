package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //DONE: Complete me
    @Override
    public String attack() {
        return "Magic!";
    }

    @Override
    public String getType() {
        return "Attack with magic";
    }
}
